const { hooks } = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
    const View = use('View')

    View.global('_common', function (field) {
        const Config = use('Config')
        return Config.get(`common.${field}`)
    })

    View.global('currentTime', function () {
        return new Date().getTime()
    })
})