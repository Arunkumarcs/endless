'use strict'

const Env = use('Env')

module.exports = {
    baseURL: Env.get('APP_URL')
}