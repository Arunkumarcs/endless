'use strict'

class IndexController {
    index({ session }) {
        return 'Dummy response'
    }
}

module.exports = IndexController